from django.http.response import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.views import generic
from rest_framework import viewsets
from rest_framework.generics import UpdateAPIView

from apps.models import *
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . serializers import *
from rest_framework import generics
# Create your views here.

class postdetailsallApi(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = postdetails.objects.all()
        serializer = postdetailsSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = postdetailsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class postdetailscodeApi(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = postdetails.objects.filter(postalcode='')
        serializer = postdetailsSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = postdetailsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class postDetails(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return postdetails.objects.get(pk=pk)
        except postdetails.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = postdetailsSerializer(snippet)
        return Response(serializer.data)

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = postdetailsSerializer(snippet, data=request.data)
        print("hello api")
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = postdetailsSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class luminaallApi(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        snippets = Lumina.objects.all()
        serializer = luminaSerializer(snippets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = luminaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class luminafilterApi(generics.ListAPIView):
    """
    List all snippets, or create a new snippet.
    """
    serializer_class = luminaSerializer
    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        city = self.kwargs['city']
        return Lumina.objects.filter(city__contains=city)


class postfilterApi(generics.ListAPIView):
    """
    List all snippets, or create a new snippet.
    """
    serializer_class = luminaSerializer
    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        ptitle = self.kwargs['ptitle']
        ptitle = ptitle.split('-')
        ptitle = ' '.join(ptitle)
        print(ptitle)
        print(postdetails.objects.filter(posttitle__contains=ptitle))
        return postdetails.objects.filter(posttitle__contains=ptitle)


class luminaDetails(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return Lumina.objects.get(pk=pk)
        except Lumina.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = luminaSerializer(snippet)
        return Response(serializer.data)

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = luminaSerializer(snippet, data=request.data)
        print("hello api")
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = luminaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


