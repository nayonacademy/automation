from django.conf.urls import url, include
from rest_framework import routers
from apps.app_views import *
router = routers.DefaultRouter()


urlpatterns = [
    url(r'^postall/$', postdetailsallApi.as_view(), name='postall'),
    url(r'^postcode/$', postdetailscodeApi.as_view(), name='postcode'),
    url(r'^postall/(?P<pk>[0-9]+)/$', postDetails.as_view(), name='postdetails'),

    url(r'^luminall/$', luminaallApi.as_view(), name='luminall'),
    url(r'^luminall/(?P<pk>[0-9]+)/$', luminaDetails.as_view(), name='luminadetails'),
    url('^cityfilter/(?P<city>.+)/$', luminafilterApi.as_view()),
    url('^ptitle/(?P<ptitle>.+)/$', postfilterApi.as_view()),
]
