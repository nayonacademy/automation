from rest_framework import serializers
from apps.models import *


class postdetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = postdetails
        fields = '__all__'

class luminaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lumina
        fields = '__all__'

