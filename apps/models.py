from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class postdetails(models.Model):
    loginurl = models.CharField(max_length=255, default='', blank=True)
    email = models.CharField(max_length=45, default='', blank=True)
    password = models.CharField(max_length=45, default='', blank=True)
    shortlink = models.CharField(blank=True, default='', max_length=45)
    posttitle = models.CharField(max_length=45, default='', blank=True)
    state = models.CharField(max_length=45, default='', blank=True)
    location = models.CharField(max_length=45, default='', blank=True)
    postalcode = models.CharField(max_length=45, default='', blank=True)
    postbody = models.CharField(max_length=45, default='', blank=True)
    height = models.CharField(max_length=45, default='', blank=True)
    body = models.CharField(max_length=45, default='2', blank=True)
    status = models.CharField(max_length=45, default='', blank=True)
    age = models.CharField(max_length=45, default='', blank=True)
    uptime = models.CharField(max_length=45, default='', blank=True)
    confirmlink = models.CharField(max_length=45, default='', blank=True)
    verify = models.CharField(max_length=45, default='', blank=True)
    errorstage = models.CharField(max_length=45, default='', blank=True)
    type = models.CharField(max_length=45, default='', blank=True)
    lock = models.CharField(max_length=45, default='', blank=True)
    usr = models.CharField(max_length=45,default= '', blank=True)

    def __str__(self):
        return self.posttitle


class Lumina(models.Model):
    ipscity = models.CharField(max_length=45, default='')
    passw = models.CharField(max_length=45, default='')
    state = models.CharField(max_length=45, default='')
    city = models.CharField(max_length=45, default='')

    def __str__(self):
        return self.city
