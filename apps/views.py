from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
import re, csv, json
# Create your views here.
from django.urls.base import reverse
from django.views.generic.base import View
from django.contrib.auth.models import User
from django.views.generic.list import ListView
from time import sleep
import datetime
import email
import imaplib
import mailbox
from bs4 import BeautifulSoup
from selenium import webdriver
from apps.models import *


class Login(View):

    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            usrcount = User.objects.count()

            login(request, user)
            return render(request, 'dashboard.html',{'user':user,'usrcount':usrcount})
        else:
            return HttpResponse("login")


class Logout(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse('login'))


class AddNewUer(View):

    template_name = 'newuser.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        user = User.objects.create_user(username, email, password)
        user.save()
        return HttpResponseRedirect(reverse('dashboard'))


class ShowAllUser(ListView):
    model = User
    template_name = 'showalluser.html'


class Dashboard(View):
    template_name = 'dashboard.html'

    def get(self, request, *args, **kwargs):
        usrcount = User.objects.count()
        allpost = postdetails.objects.all()
        return render(request, self.template_name,{'usrcount':usrcount,'allpost':allpost})


class NpvaUpload(View):
    template_name = 'npvaupload.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):

        file = request.FILES['upload_file']
        decoded_file = file.read().decode('utf-8').splitlines()
        reader = csv.DictReader(decoded_file)
        for row in reader:
            # print(row.keys())
            # ['height', 'status', 'emailAddress', 'posting body', 'Posting Title', 'age', 'specific location', 'body',
            #  'postal code']

            if 'height' in row.keys():
                height = row['height']
            else:
                height = ''

            if 'status' in row.keys():
                status = row['status']
            else:
                status = ''

            if 'emailAddress' in row.keys():
                email = row['emailAddress']
            else:
                email = ''

            if 'posting body' in row.keys():
                shortlink = row['posting body']
            else:
                shortlink = ''

            if 'Posting Title' in row.keys():
                posttitle = row['Posting Title']
            else:
                posttitle = ''

            if 'age' in row.keys():
                age = row['age']
            else:
                age = ''

            if 'specific location' in row.keys():
                location = row['specific location']
            else:
                location = ''

            if 'body' in row.keys():
                postbody = row['body']
            else:
                postbody = ''

            if 'postal code' in row.keys():
                postalcode = row['postal code']
            else:
                postalcode = ''

            if 'loginurl' in row.keys():
                loginurl = row['loginurl']
            else:
                loginurl = ''

            if 'password' in row.keys():
                password = row['password']
            else:
                password = ''

            if 'shortlink' in row.keys():
                shortlink = row['shortlink']
            else:
                shortlink = ''

            if 'status' in row.keys():
                status = row['status']
            else:
                status = ''

            if 'uptime' in row.keys():
                uptime = row['uptime']
            else:
                uptime = ''

            if 'confirmlink' in row.keys():
                confirmlink = row['confirmlink']
            else:
                confirmlink = ''

            if 'verify' in row.keys():
                verify = row['verify']
            else:
                verify = ''

            if 'errorstage' in row.keys():
                errorstage = row['errorstage']
            else:
                errorstage = ''

            if 'type' in row.keys():
                type = row['type']
            else:
                type = ''

            if 'lock' in row.keys():
                lock = row['lock']
            else:
                lock = ''

            userid = User.objects.filter(id=request.user.id).get()
            postdetails(
                loginurl = loginurl,
                email = email,
                password = password,
                shortlink = shortlink,
                posttitle = posttitle,
                location = location,
                postalcode = postalcode,
                postbody = postbody,
                height = height,
                status = status,
                age = age,
                uptime = uptime,
                confirmlink = confirmlink,
                verify = verify,
                errorstage = errorstage,
                type = type,
                lock = lock,
                usr = request.user.id
            )
            postdetails.objects.create(
                loginurl = loginurl,
                email = email,
                password = password,
                shortlink = shortlink,
                posttitle = posttitle,
                location = location,
                postalcode = postalcode,
                postbody = postbody,
                height = height,
                status = status,
                age = age,
                uptime = uptime,
                confirmlink = confirmlink,
                verify = verify,
                errorstage = errorstage,
                type = type,
                lock = lock,
                usr = request.user.id)
            # print(userid.first().pk)

        return render(request, self.template_name)


class PvaUpload(View):
    template_name = 'pvaupload.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):

        file = request.FILES['upload_file']
        decoded_file = file.read().decode('utf-8').splitlines()
        reader = csv.DictReader(decoded_file)
        for row in reader:
            # print(row.keys())
            # ['height', 'status', 'emailAddress', 'posting body', 'Posting Title', 'age', 'specific location', 'body',
            #  'postal code']

            if 'height' in row.keys():
                height = row['height']
            else:
                height = ''

            if 'status' in row.keys():
                status = row['status']
            else:
                status = ''

            if 'email' in row.keys():
                email = row['email']
            else:
                email = ''

            if 'posting body' in row.keys():
                postbody = row['posting body']
            else:
                postbody = ''

            if 'Posting Title' in row.keys():
                posttitle = row['Posting Title']
            else:
                posttitle = ''

            if 'age' in row.keys():
                age = row['age']
            else:
                age = ''

            if 'specific location' in row.keys():
                location = row['specific location']
            else:
                location = ''

            if 'body' in row.keys():
                body = row['body']
            else:
                body = ''

            if 'postal code' in row.keys():
                postalcode = row['postal code']
            else:
                postalcode = ''

            if 'loginurl' in row.keys():
                loginurl = row['loginurl']
            else:
                loginurl = ''

            if 'password' in row.keys():
                password = row['password']
            else:
                password = ''

            if 'shortlink' in row.keys():
                shortlink = row['shortlink']
            else:
                shortlink = ''

            if 'uptime' in row.keys():
                uptime = row['uptime']
            else:
                uptime = ''

            if 'confirmlink' in row.keys():
                confirmlink = row['confirmlink']
            else:
                confirmlink = ''

            if 'verify' in row.keys():
                verify = row['verify']
            else:
                verify = ''

            if 'errorstage' in row.keys():
                errorstage = row['errorstage']
            else:
                errorstage = ''

            if 'type' in row.keys():
                type = row['type']
            else:
                type = ''

            if 'lock' in row.keys():
                lock = row['lock']
            else:
                lock = ''

            if 'state' in row.keys():
                state = row['state']
            else:
                state = ''

            userid = User.objects.filter(id=request.user.id).get()
            postdetails(
                loginurl = loginurl,
                email = email,
                password = password,
                shortlink = shortlink,
                posttitle = posttitle,
                location = location,
                postalcode = postalcode,
                postbody = postbody,
                height = height,
                status = status,
                age = age,
                body = body,
                uptime = uptime,
                confirmlink = confirmlink,
                verify = verify,
                errorstage = errorstage,
                type = type,
                lock = lock,
                state = state,
                usr = request.user.id
            )
            postdetails.objects.create(
                loginurl = loginurl,
                email = email,
                password = password,
                shortlink = shortlink,
                posttitle = posttitle,
                location = location,
                postalcode = postalcode,
                postbody = postbody,
                height = height,
                status = status,
                age = age,
                body = body,
                uptime = uptime,
                confirmlink = confirmlink,
                verify = verify,
                errorstage = errorstage,
                type = type,
                lock = lock,
                state = state,
                usr = request.user.id)
            # print(userid.first().pk)
        return HttpResponseRedirect(reverse('currentstatus'))


class LuminaUpload(View):
    template_name = 'luminaupload.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):

        file = request.FILES['upload_file']
        decoded_file = file.read().decode('utf-8').splitlines()
        reader = csv.DictReader(decoded_file)
        for row in reader:
            # print(row.keys())
            # ['height', 'status', 'emailAddress', 'posting body', 'Posting Title', 'age', 'specific location', 'body',
            #  'postal code']

            if 'Ips city' in row.keys():
                ips = row['Ips city']
            else:
                ips = ''

            if 'Ips password ' in row.keys():
                passw = row['Ips password ']
            else:
                passw = ''

            print(ips,passw)
            ipsstr = ips.split('-')
            state = ipsstr[8]
            city = ipsstr[10]
            print(ipsstr)
            print(state,city)
            Lumina(
                ipscity = ips,
                passw = passw,
                state = state,
                city = city,
            )
            Lumina.objects.create(
                ipscity = ips,
                passw = passw,
                state = state,
                city = city,
            )
            # print(userid.first().pk)
        return HttpResponseRedirect(reverse('currentstatus'))


class LuminaList(View):
    template_name = 'luminalist.html'

    def get(self, request, *args, **kwargs):
        allpost = Lumina.objects.all()
        context = {
            'allpost':allpost
        }
        return render(request, self.template_name, context)


class LuminaListdelete(View):

    def get(self, request, *args, **kwargs):
        id = kwargs['id']
        allpost = Lumina.objects.filter(pk=id)
        allpost.delete()
        return HttpResponseRedirect(reverse('luminalist'))


class CurrentStatus(View):
    template_name = 'currentstatus.html'

    def get(self, request, *args, **kwargs):
        allpost = postdetails.objects.all()
        context = {
            'allpost':allpost
        }
        return render(request, self.template_name, context)


class CurrentStatusdelete(View):

    def get(self, request, *args, **kwargs):
        id = kwargs['id']
        allpost = postdetails.objects.filter(pk=id)
        allpost.delete()
        return HttpResponseRedirect(reverse('currentstatus'))


class CurrentStatusupdate(View):
    template_name = 'currentstatusupdate.html'

    def get(self, request, *args, **kwargs):
        id = kwargs['id']
        allpost = postdetails.objects.filter(pk=id)
        return render(request,self.template_name,{'allpost':allpost})

    def post(self, request, *args, **kwargs):
        id = kwargs['id']
        email =request.POST.get('email','')
        password =request.POST.get('password','')
        shortlink =request.POST.get('shortlink','')
        posttitle =request.POST.get('posttitle','')
        state =request.POST.get('state','')
        location =request.POST.get('location','')
        postalcode =request.POST.get('postalcode','')
        postbody =request.POST.get('postbody','')
        height =request.POST.get('height','')
        body =request.POST.get('body','')
        status =request.POST.get('status','')
        age =request.POST.get('age','')
        confirmlink =request.POST.get('confirmlink','')
        type =request.POST.get('type','')
        lock =request.POST.get('lock','')

        allpost = postdetails.objects.filter(pk=id)
        allpost.update(
            posttitle = posttitle,
            email =email,
            password =password,
            shortlink =shortlink,
            state =state,
            location =location,
            postalcode =postalcode,
            postbody =postbody,
            height =height,
            body =body,
            status =status,
            age =age,
            confirmlink =confirmlink,
            type =type,
            lock =lock,
        )
        return HttpResponseRedirect(reverse('currentstatus'))

def mailconfirm(request):
    EMAIL_ACCOUNT = "112@twomx.com"
    PASSWORD = "Suvo1112"
    mail = imaplib.IMAP4_SSL('box.two.twomx.com')

    mail.login(EMAIL_ACCOUNT, PASSWORD)
    mail.list()
    mail.select('inbox')
    # result, data = mail.uid('search', None, "UNSEEN") # (ALL/UNSEEN)
    result, data = mail.uid('search', None, '(FROM "robot@craigslist.org")')
    i = len(data[0].split())

    confirm = []

    for x in range(i):
        latest_email_uid = data[0].split()[x]
        result, email_data = mail.uid('fetch', latest_email_uid, '(RFC822)')
        # result, email_data = conn.store(num,'-FLAGS','\\Seen')
        # this might work to set flag to seen, if it doesn't already
        raw_email = email_data[0][1]
        raw_email_string = raw_email.decode('utf-8')
        email_message = email.message_from_string(raw_email_string)

        # Header Details
        date_tuple = email.utils.parsedate_tz(email_message['Date'])
        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            local_message_date = "%s" % (str(local_date.strftime("%a, %d %b %Y %H:%M:%S")))
        email_from = str(email.header.make_header(email.header.decode_header(email_message['From'])))
        email_to = str(email.header.make_header(email.header.decode_header(email_message['To'])))
        subject = str(email.header.make_header(email.header.decode_header(email_message['Subject'])))
        # Body details
        for part in email_message.walk():
            if part.get_content_type() == "text/html":
                body = part.get_payload()
                # file_name = "email_" + str(x) + ".txt"
                # output_file = open(file_name, 'w')
                # output_file.write("From: %s\nTo: %s\nDate: %s\nSubject: %s\n\nBody: \n\n%s" %(email_from, email_to,local_message_date, subject, body.decode('utf-8')))
                # output_file.close()
                # print(body)
                soup = BeautifulSoup(body, 'html.parser')
                urllink = soup.a['href']
            else:
                continue
        confirm.append({'subject': subject, 'url': urllink})
        postti = str(subject)
        postti = postti.strip().split(':')[1].split('(')[0]
        postti = postti.replace(',', '').split()
        similr = postdetails.objects.filter(posttitle__contains=postti).update(confirmlink=urllink)
        print(subject, urllink)
        print(similr)
    return HttpResponse("done")
