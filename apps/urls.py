from django.conf.urls import url

from apps import views
from apps.views import *

urlpatterns = [
    url(r'^$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^addnewuser/$', AddNewUer.as_view(), name='addnewuser'),
    url(r'^showalluser/$', ShowAllUser.as_view(), name='showalluser'),
    url(r'^dashboard/$', Dashboard.as_view(), name='dashboard'),
    url(r'^npvaupload/$', NpvaUpload.as_view(), name='npvaupload'),
    url(r'^pvaupload/$', PvaUpload.as_view(), name='pvaupload'),
    url(r'^luminaupload/$', LuminaUpload.as_view(), name='luminaupload'),
    url(r'^luminalist/$', LuminaList.as_view(), name='luminalist'),
    url(r'^luminalistdelete/(?P<id>\d+)$', LuminaListdelete.as_view(), name='luminalistdelete'),
    url(r'^currentstatus/$', CurrentStatus.as_view(), name='currentstatus'),
    url(r'^currentstatusdelete/(?P<id>\d+)$', CurrentStatusdelete.as_view(), name='currentstatusdelete'),
    url(r'^currentstatusupdate/(?P<id>\d+)$', CurrentStatusupdate.as_view(), name='currentstatusupdate'),
    url(r'^mailconfirm/$', views.mailconfirm, name='mailconfirm'),
]