FROM python:3.5-onbuild

RUN apt-get update
RUN apt-get install -y libmysqlclient-dev mysql-client sqlite3 postgresql-client libpq-dev libffi-dev
RUN pip install -r requirements.txt
ENV PYTHONUNBUFFERED 1
